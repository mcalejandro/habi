from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from properties.serializers import PropertySerializer
from properties.models import Property


class PropertyViewSet(viewsets.ModelViewSet):
    queryset = Property.objects.filter(statushistory__status__in=[3, 4, 5])
    serializer_class = PropertySerializer
    filter_backends = [DjangoFilterBackend,]
    filter_fields = ['year', 'city', 'status']
