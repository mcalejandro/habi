from rest_framework import serializers
from properties.models import Property, Status, StatusHistory


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ['name', 'label']


class PropertySerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Property
        fields = ['pk', 'address', 'price', 'description', 'status']

    def get_status(self, obj):
        last_status = StatusHistory.objects.filter(property=obj).last().status
        serializer_data = StatusSerializer(last_status, many=False).data
        return serializer_data

