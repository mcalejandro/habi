from properties.viewsets import PropertyViewSet
from rest_framework.routers import DefaultRouter
from django.urls import include, path

router = DefaultRouter()
router.register(r'properties', PropertyViewSet , basename='properties')

urlpatterns = [
   path('', include(router.urls)),
]