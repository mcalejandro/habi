import pytest
from properties.models import Property

pytestmark = pytest.mark.django_db


class TestProperty:
    """
    Test Property Model
    """

    pytestmark = pytest.mark.django_db
    def test_creation(self, property_factory,):
        """
        test created property
        """
        propety_created = property_factory()
        property = Property.objects.get(id = propety_created.id)
        assert property.address == "Calle prueba, 90, test unitarios, 22216", \
            "address should be Calle prueba, 90, test unitarios, 22216"
        assert property.city == "Tijuana", "city should ve Tijuana"
        assert property.price == 2000000, "price should be 20000"
        assert property.description == "Esta es una descripcion para los test unitarios", \
            "description should be Esta es una descripcion para los test unitarios"
        assert property.year == 2018, "year should be 2018"


    pytestmark = pytest.mark.django_db
    def test_update(self, property_factory,):
        """
        test updated property
        """
        propety_created = property_factory()
        property = Property.objects.get(id = propety_created.id)

        property.price == 1000000
        property.description == "Esta es una descripcion para los test unitarios de UPDATE"
        property.save()
        assert property.price == 1000000, 'price should be 1000000'
        assert property.description == "Esta es una descripcion para los test unitarios de UPDATE", \
            'description should be "Esta es una descripcion para los test unitarios de UPDATE"'