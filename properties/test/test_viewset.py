import json
from django.urls import reverse
from properties.models import Property
from properties.viewsets import PropertyViewSet


class TestPropertyViewSet:

    def test_get(self, rf, mocker):
        """
        test create
        """
        url = reverse('properties')

        data = {"city": 'medellin', "year": "2018", "description": "descripion text", "price": 20000}

        request = rf.post(url,
                          content_type='application/json',
                          data=json.dumps(data))

        mocker.patch.object(Property, 'save')
        response = PropertyViewSet.as_view({'post': 'create'})(request).render()
        assert response.status_code == 201
        assert json.loads(response.content).get('name') == 'profile_test'
        assert Property.save.called
