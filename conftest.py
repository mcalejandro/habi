"""
Confest
"""
import pytest
from pytest_factoryboy import register
from .factories import PropertyFactory

register(PropertyFactory)

@pytest.fixture()
def post(property_factory,):
    """
    return fixture to use in tests
    """
    return property_factory,()

