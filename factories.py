"""
Factories
"""
import factory
from faker import Factory as FakerFactory
from properties.models import Property

faker = FakerFactory.create()

class PropertyFactory(factory.django.DjangoModelFactory):
    """
    Default Factory for Property
    """
    class Meta:
        """
        Select Model Profile
        """
        model = Property

    name = "property_test"
    key = factory.LazyAttribute(lambda x: faker.name())
    address = factory.LazyAttribute(lambda x: faker.text())
    city = factory.LazyAttribute(lambda x: faker.text())
    price = factory.LazyAttribute(lambda x: faker.int())
    description = factory.LazyAttribute(lambda x: faker.text())
    year = factory.LazyAttribute(lambda x: faker.int())